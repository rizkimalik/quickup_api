const knex = require('../../config/db_connect');
const { response, logger } = require('../../helper');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { category_sublv2_id } = req.params;
        let data = '';
        if (category_sublv2_id === 'all') {
            data = await knex('category_sub_lv3');
        }
        else {
            data = await knex('category_sub_lv3').where({ category_sublv2_id });
        }
        for (let i = 0; i < data.length; i++) {
            const category = await knex('category').where({ category_id: data[i].category_id }).select('name').first();
            const sublv1 = await knex('category_sub_lv1').where({ category_sublv1_id: data[i].category_sublv1_id }).select('sub_name').first();
            const sublv2 = await knex('category_sub_lv2').where({ category_sublv2_id: data[i].category_sublv2_id }).select('sub_name').first();
            const department = await knex('departments').where({ id: data[i].department_id }).select('department_name').first();
            data[i].category_name = category?.name;
            data[i].category_sublv1_name = sublv1?.sub_name;
            data[i].category_sublv2_name = sublv2?.sub_name;
            data[i].department_name = department?.department_name;
        }
        response.ok(res, data);
    }
    catch (error) {
        logger('category_sub_lv3/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { category_sublv3_id } = req.params;
        const category = await knex('category_sub_lv3').where({ category_sublv3_id });
        response.ok(res, category);
    }
    catch (error) {
        logger('category_sub_lv3/show', error);
        res.status(500).end();
    }
}


const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            category_id,
            category_sublv1_id,
            category_sublv2_id,
            sub_name,
            description,
            sla,
            department_id
        } = req.body;
        const subcategory3 = await knex('category_sub_lv3')
            .select('category_sublv3_id')
            .orderBy('category_sublv3_id', 'desc').first();

        let categorysublv3_id;
        if (Boolean(subcategory3) === false) {
            categorysublv3_id = `CT3-30001`;
        }
        else {
            const categorysublv3_no = Number(subcategory3.category_sublv3_id.split('-')[1]) + 1;
            categorysublv3_id = `CT3-${categorysublv3_no}`;
        }

        await knex('category_sub_lv3')
            .insert([{
                category_id,
                category_sublv1_id,
                category_sublv2_id,
                category_sublv3_id: categorysublv3_id,
                sub_name,
                description,
                sla,
                department_id
            }]);
        response.ok(res, categorysublv3_id);
    }
    catch (error) {
        logger('category_sub_lv3/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            category_id,
            category_sublv1_id,
            category_sublv2_id,
            category_sublv3_id,
            sub_name,
            description,
            sla,
            department_id
        } = req.body;

        await knex('category_sub_lv3')
            .update({
                category_id,
                category_sublv1_id,
                category_sublv2_id,
                sub_name,
                description,
                sla,
                department_id
            })
            .where({ category_sublv3_id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('category_sub_lv3/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { category_sublv3_id } = req.params;
        const res_delete = await knex('category_sub_lv3').where({ category_sublv3_id }).del();
        response.ok(res, res_delete);
    }
    catch (error) {
        logger('category_sub_lv3/destroy', error);
        res.status(500).end();
    }
}

const detail_sublv3 = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { category_sublv3_id, customer_id } = req.body;

        const result = await knex('category_sub_lv3').where({ category_sublv3_id }).first();
        const detail = await knex('category_sub_lv3_customer').where({ category_sublv3_id, customer_id }).select('sla').first();
        result.sla = detail ? detail.sla : result.sla; //get sla assigned

        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/category_sub_lv3/detail_sublv3', error.message);
        res.status(500).end();
    }
}

const assigned_customer_sla = async (req, res) => {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { category_sublv3_id, skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT a.*,b.name FROM category_sub_lv3_customer a 
            LEFT JOIN customers b ON a.customer_id=b.customer_id 
            WHERE category_sublv3_id='${category_sublv3_id}'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM category_sub_lv3_customer a 
            LEFT JOIN customers b ON a.customer_id=b.customer_id WHERE category_sublv3_id='${category_sublv3_id}' 
            ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ERROR/category_sub_lv3/assigned_customer_sla', error.message);
    }

}

const insert_customer_sla = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            category_sublv3_id,
            customer_id,
            sla,
            created_by,
            active
        } = req.body;

        let result = '';
        const validation = await knex('category_sub_lv3_customer').where({ customer_id }).first();
        if (!validation) {
            result = await knex('category_sub_lv3_customer')
                .insert({
                    category_sublv3_id,
                    customer_id,
                    sla,
                    created_by,
                    created_at: knex.fn.now(),
                    active
                });
        } 
        else {
            result = 'allready exist.'
        }

        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/category_sub_lv3/insert_customer_sla', error.message);
    }
}

const update_customer_sla = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            category_sublv3_id,
            customer_id,
            sla,
            updated_by,
            active
        } = req.body;

        const result = await knex('category_sub_lv3_customer')
            .update({
                category_sublv3_id,
                customer_id,
                sla,
                updated_by,
                updated_at: knex.fn.now(),
                active
            })
            .where({ id });
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/category_sub_lv3/update', error.message);
    }
}

const delete_customer_sla = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('category_sub_lv3_customer').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        logger('ERROR/category_sub_lv3/delete_customer_sla', error.message);
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
    detail_sublv3,
    assigned_customer_sla,
    insert_customer_sla,
    update_customer_sla,
    delete_customer_sla,
}