'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { response } = require('../../helper');

const report_summary_grid = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { sort, filter, date_start, date_end } = req.body;
        let datasort = sort ?? '';
        let orderby = 'ORDER BY username, user_level ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        const result = await knex.raw(`
            SELECT u.username, u.user_level, d.department_name, COUNT(ti.ticket_number) AS total_handled
            FROM users u
            LEFT JOIN (
                SELECT DISTINCT user_create, ticket_number
                FROM ticket_interactions
                WHERE status='Closed' AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(created_at,'%Y-%m-%d') <= '${date_end}'
            ) ti ON u.username = ti.user_create
            LEFT JOIN departments d ON u.department = d.id
            WHERE u.user_level NOT IN ('Admin', 'SPV')
            GROUP BY u.username 
            ${orderby}
        `);
        
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/report_summary_grid', error);
        res.status(500).end();
    }
}

const report_summary_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end } = req.body;
        const a = new Date(date_start);
        const b = new Date(date_end);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;
        const max = 60;

        if (days <= max + 1) {
            const result = await knex.raw(`
                SELECT u.username, u.user_level, d.department_name, COUNT(ti.ticket_number) AS total_handled
                FROM users u
                LEFT JOIN (
                    SELECT DISTINCT user_create, ticket_number
                    FROM ticket_interactions
                    WHERE status='Closed' AND DATE_FORMAT(created_at,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(created_at,'%Y-%m-%d') <= '${date_end}'
                ) ti ON u.username = ti.user_create
                LEFT JOIN departments d ON u.department = d.id
                WHERE u.user_level NOT IN ('Admin', 'SPV')
                GROUP BY u.username ORDER BY username, user_level ASC
            `);
            response.ok(res, result[0]);
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range ${max} days.`
            });
            res.end();
        }
    }
    catch (error) {
        response.data(res, error.message, 'report/report_summary_export');
    }
}

module.exports = { report_summary_grid, report_summary_export }