'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const agent_handle_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;
        const result = await knex.raw(`CALL sp_report_agent_channel('${action}')`);
        response.ok(res, result[0][0]);
    }
    catch (error) {
        logger('report/agent_handle_channel', error);
        res.status(500).end();
    }
}

const data_channel_chart = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;
        const result = await knex.raw(`CALL sp_report_total_channel('${action}')`);
        response.ok(res, result[0][0]);
    }
    catch (error) {
        logger('report/data_channel_chart', error);
        res.status(500).end();
    }
}



module.exports = { agent_handle_channel, data_channel_chart }