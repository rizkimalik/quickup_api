'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const report_by_agent_grid = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, date_start, date_end, agent_handle, agent_layer, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY date_create DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        let param_ticket, param_total;
        if (agent_layer === 'L1') {
            param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND user_create='${agent_handle}' ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND user_create='${agent_handle}' ${filter_by_customer} ${filtering}`;
        }
        else if (agent_layer === 'L2') {
            param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L2' AND user_create='${agent_handle}') ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L2' AND user_create='${agent_handle}') ${filter_by_customer} ${filtering}`;
        }
        else if (agent_layer === 'L3') {
            param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L3' AND user_create='${agent_handle}') ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L3' AND user_create='${agent_handle}') ${filter_by_customer} ${filtering}`;
        }
        else {
            //? agent_layer = admin
            param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} ${filtering}`;
        }

        if (agent_layer === 'Admin') {
            const tickets = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets ${param_ticket}`);
            const total = await knex.raw(`SELECT COUNT(*) AS total from view_tickets ${param_total}`);
            response.data(res, tickets[0], total[0][0].total);
        } else {
            const tickets = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets ${param_ticket}`);
            const total = await knex.raw(`SELECT COUNT(*) AS total from view_tickets ${param_total}`);
            response.data(res, tickets[0], total[0][0].total);
        }
    }
    catch (error) {
        logger('report/report_by_agent_grid', error);
        res.status(500).end();
    }
}

const report_by_agent_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end, agent_handle, agent_layer, customer_id } = req.body;
        const a = new Date(date_start);
        const b = new Date(date_end);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;
        const max = 60;

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        if (days <= max + 1) {
            let param_ticket = '';
            if (agent_layer === 'L1') {
                param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND user_create='${agent_handle}' ${filter_by_customer}`;
            }
            else if (agent_layer === 'L2') {
                param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L2' AND user_create='${agent_handle}')`;
            }
            else if (agent_layer === 'L3') {
                param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L3' AND user_create='${agent_handle}')`;
            }
            else {
                //? agent_layer = admin
                param_ticket = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer}`;
            }

            const tickets = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create FROM view_tickets ${param_ticket}`);
            response.ok(res, tickets[0]);
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range ${max} days.`
            });
            res.end();
        }
    }
    catch (error) {
        logger('report/report_by_agent_export', error);
        res.status(500).end();
    }
}

module.exports = { report_by_agent_grid, report_by_agent_export }