const report_sla = require('./report_sla');
const report_interaction = require('./report_interaction_ticket');
const report_onprogress = require('./report_onprogress');
const report_by_agent = require('./report_by_agent');
const agent_handle_channel = require('./agent_handle_channel');
const speed_answer_call = require('./speed_answer_call');
const speed_answer_messages = require('./speed_answer_messages');
const report_sentiment_analysis = require('./report_sentiment_analysis');
const report_monthly_ticket = require('./report_monthly_ticket');
const report_summary = require('./report_summary');

module.exports = {
    report_sla,
    report_interaction,
    report_onprogress,
    report_by_agent,
    agent_handle_channel,
    speed_answer_call,
    speed_answer_messages,
    report_sentiment_analysis,
    report_monthly_ticket,
    report_summary,
}