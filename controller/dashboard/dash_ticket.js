'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const logger = require('../../helper/logger');
const response = require('../../helper/json_response');

const total_ticket = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { department_id, user_create, user_level, action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(date_create) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        let tickets;
        if (user_level === 'L1') {
            tickets = await knex.raw(`SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status AND department_id='${department_id}' AND user_create='${user_create}' AND ${date_range}) as total FROM status WHERE active='1'`);
        }
        else if (user_level === 'L2') {
            tickets = await knex.raw(`
                SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status 
                AND dispatch_department='${department_id}' AND ticket_number IN (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L2') AND ${date_range}
                ) as total FROM status WHERE active='1'
            `);
        }
        else if (user_level === 'L3') {
            tickets = await knex.raw(`
                SELECT status,icon, (select count(ticket_number) FROM view_tickets 
                where status=status.status AND dispatch_department='${department_id}' 
                AND ticket_number IN (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND dispatch_to_layer='L3')
                AND  ${date_range}
                ) as total FROM status WHERE active='1'
            `);
        }
        else {
            //? user_level = admin
            tickets = await knex.raw(`SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status AND ${date_range}) as total FROM status WHERE active='1'`);
        }

        if (process.env.DB_CONNECTION === 'mysql') {
            response.ok(res, tickets[0]);
        }
        else {
            response.ok(res, tickets);
        }
    }
    catch (error) {
        logger('dash_ticket/total_ticket', error);
        res.status(500).end();
    }
}

const chart_by_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(date_create) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(date_create) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const channels = await knex('channels').select('channel').groupBy('channel');
        for (let i = 0; i < channels.length; i++) {
            let tota_data = await knex.raw(`SELECT COUNT(*) as total FROM view_tickets WHERE ticket_source='${channels[i].channel}' AND ${date_range}`);
            channels[i].total = tota_data[0][0].total;
        }
        response.ok(res, channels);
    }
    catch (error) {
        logger('dash_ticket/chart_by_channel', error);
        res.status(500).end();
    }
}

const chart_by_total = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let result = '';
        if (action === 'Today') {
            result = await knex.raw(`
                SELECT CONCAT(t.label_name,':00') as label_name, SUM( t.total_ticket ) AS total_ticket
                FROM (
                SELECT HOUR ( date_create ) AS label_name, COUNT( ticket_number ) AS total_ticket
                FROM tickets where DATE(date_create)=DATE(NOW())
                GROUP BY HOUR ( date_create ) 
                    UNION ALL
                SELECT HOUR ( date_create ) AS label_name, COUNT( ticket_number ) AS total_ticket 
                FROM ticket_closed where DATE(date_create)=DATE(NOW())
                GROUP BY HOUR ( date_create ) 
                ) AS t 
                GROUP BY t.label_name
            `);
        }
        else if (action === 'Last7Days') {
            result = await knex.raw(`
                SELECT t.label_name, t.date_create, SUM( t.total_ticket ) AS total_ticket
                FROM (
                    SELECT 
                        DAYNAME( date_create ) AS label_name, 
                        DATE ( date_create ) AS date_create, 
                        COUNT( ticket_number ) AS total_ticket
                    FROM tickets GROUP BY DATE ( date_create ) 
                        UNION ALL
                    SELECT
                        DAYNAME( date_create ) AS label_name,
                        DATE ( date_create ) AS date_create,
                        COUNT( ticket_number ) AS total_ticket 
                    FROM ticket_closed GROUP BY DATE ( date_create ) 
                ) AS t 
                GROUP BY t.label_name, t.date_create 
                ORDER BY t.date_create LIMIT 7
            `);
        }

        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_ticket/chart_by_total', error);
        res.status(500).end();
    }
}

const data_ticket_sla = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(vt.date_create) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(vt.date_create) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT
            t1.ticket_number,
            DATE_FORMAT(t1.date_create,'%Y-%m-%d %H:%i:%s') AS date_create,
            DATE_FORMAT(t1.date_closed,'%Y-%m-%d %H:%i:%s') AS date_closed,
            t1.status,
            t1.dispatch_to_layer,
            (TIMESTAMPDIFF( SECOND, t1.L1_min, t1.L1_max )/ 86400) AS L1_diff_day,
            (TIMESTAMPDIFF( SECOND, t1.L2_min, t1.L2_max ) / 86400) AS L2_diff_day,
            (TIMESTAMPDIFF( SECOND, t1.L3_min, t1.L3_max ) / 86400) AS L3_diff_day,
            t1.sla,
            CEILING(TIMESTAMPDIFF( SECOND, t1.L1_min, t1.L1_max )/ 86400)+
            CEILING(TIMESTAMPDIFF( SECOND, t1.L2_min, t1.L2_max )/ 86400)+
            CEILING(TIMESTAMPDIFF( SECOND, t1.L3_min, t1.L3_max )/ 86400)   
            as 'total_sla' ,
                CASE
                WHEN CEILING(TIMESTAMPDIFF( SECOND, t1.L1_min, t1.L1_max )/ 86400)+
                    CEILING(TIMESTAMPDIFF( SECOND, t1.L2_min, t1.L2_max )/ 86400)+
                    CEILING(TIMESTAMPDIFF( SECOND, t1.L3_min, t1.L3_max )/ 86400) <= t1.sla
                THEN 100
                ELSE CEILING(t1.sla / (CEILING(TIMESTAMPDIFF( SECOND, t1.L1_min, t1.L1_max )/ 86400)+
                            CEILING(TIMESTAMPDIFF( SECOND, t1.L2_min, t1.L2_max )/ 86400)+
                            CEILING(TIMESTAMPDIFF( SECOND, t1.L3_min, t1.L3_max )/ 86400)) * 100)
            END AS sla_percentage
            FROM (
                SELECT
                    vt.ticket_number,
                    vt.sla,
                    vt.date_create,
                    vt.date_closed,
                    min(ti.status ) AS status,
                    max(ti.dispatch_to_layer) as dispatch_to_layer,
                    MIN( CASE WHEN ti.dispatch_to_layer = 'L1' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)  END ) AS L1_min,
                    MIN( CASE WHEN ti.dispatch_to_layer = 'L2' OR ti.dispatch_to_layer= 'L3' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)  END ) AS L1_max,
                    MIN( CASE WHEN ti.dispatch_to_layer = 'L2' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)   END ) AS L2_min,
                    MAX( CASE WHEN ti.dispatch_to_layer = 'L2' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)   END ) AS L2_max,
                    MIN( CASE WHEN ti.dispatch_to_layer = 'L3' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)   END ) AS L3_min,
                    MAX( CASE WHEN ti.dispatch_to_layer = 'L3' THEN ti.created_at ELSE if(vt.date_closed is Null , NOW(), vt.date_closed)   END ) AS L3_max 
                FROM v_tickets AS vt
                INNER JOIN ticket_interactions AS ti ON vt.ticket_number = ti.ticket_number 
                WHERE ${date_range}	
                GROUP BY vt.ticket_number 
            ) t1 ORDER BY date_create DESC
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        logger('dash_ticket/chart_by_total', error);
        res.status(500).end();
    }
}

module.exports = {
    total_ticket,
    chart_by_channel,
    chart_by_total,
    data_ticket_sla,
}