-- DROP PROCEDURE sp_auto_distribute_whatsapp
CREATE PROCEDURE sp_auto_distribute_whatsapp()
BEGIN

	DECLARE TotalChat VARCHAR(10);
	DECLARE AgentName VARCHAR(50);
	DECLARE AgentNameExist VARCHAR(50);
	DECLARE ChatID VARCHAR(50);
	DECLARE CustomerID VARCHAR(50);
	DECLARE PhoneNumber VARCHAR(50);
	
	SET TotalChat = (SELECT COUNT(*) FROM chats WHERE (agent_handle IS NULL OR agent_handle='') AND flag_end='N' AND flag_to='customer' AND channel='Whatsapp'); -- check queing

IF TotalChat <> 0 Then
    SELECT chat_id, user_id, customer_id INTO ChatID,PhoneNumber,CustomerID FROM chats 
        WHERE (agent_handle IS NULL OR agent_handle='') AND flag_end='N' AND flag_to='customer' AND channel='Whatsapp' ORDER BY id ASC LIMIT 1;
    SELECT username INTO AgentName from v_bucket_agent_whatsapp WHERE total_handle < max_whatsapp ORDER BY last_distribute_whatsapp ASC LIMIT 1;
    SELECT agent_handle INTO AgentNameExist FROM chats WHERE agent_handle <> '' AND chat_id=ChatID ORDER BY id ASC LIMIT 1;
    
    
    if AgentNameExist <> '' then
        UPDATE chats SET agent_handle=(SELECT agent_handle FROM chats WHERE agent_handle <> '' AND chat_id=ChatID ORDER BY id ASC LIMIT 1), cnt='2', date_assign=NOW()
        WHERE (agent_handle IS NULL OR agent_handle='') AND chat_id=ChatID;
        
        SELECT 'assigned' AS status, ChatID AS chat_id, CustomerID AS customer_id, PhoneNumber AS phone_number, AgentNameExist AS agent_handle, PhoneNumber + ' assign to - ' + AgentName AS message;
    Elseif AgentName <> '' then
        UPDATE chats SET agent_handle=AgentName, cnt='2', date_assign=NOW() WHERE (agent_handle IS NULL OR agent_handle='') AND chat_id=ChatID;
        UPDATE users SET last_distribute_whatsapp=NOW() WHERE username=AgentName;
        
        SELECT 'assigned' AS status, ChatID AS chat_id, CustomerID AS customer_id, PhoneNumber AS phone_number, AgentName AS agent_handle, PhoneNumber + ' assign to - ' + AgentName AS message;
    else
        SELECT 'queing' AS status, 'agent login - not available, queing : ' + TotalChat + ' message.' AS message;
		end if;
	Else
        SELECT 'distributed' AS status, 'all whatsapp message already distribute.' AS message;
	END IF;
END