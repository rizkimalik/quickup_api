
const up = function(knex) {
    return knex.schema.createTable('customer_type', function(table){
        table.increments('id').primary();
        table.string('type_name', 50).unique();
        table.text('description');
        table.boolean('active');
        table.string('created_by', 50);
        table.string('updated_by', 50);
        table.timestamps();
    })
};

const down = function(knex) {
    return knex.schema.dropTable('customer_type');
};

module.exports = {up, down}