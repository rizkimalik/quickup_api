const blending = require('../controller/blending_controller');

module.exports = function (app, io) {
    app.prefix('/blending', function (api) {
        api.route('/send_message_cust').post((req, res) => {
            blending.send_message_cust(req, res, io)
        });
        api.route('/send_message_agent').post((req, res) => {
            blending.send_message_agent(req, res, io)
        });
        api.route('/queing_chat').post((req, res) => {
            blending.queing_chat(req, res, io)
        });
        api.route('/send_message_whatsapp').post((req, res) => {
            blending.send_message_whatsapp(req, res, io)
        });
        api.route('/send_directmessage_twitter').post((req, res) => {
            blending.send_directmessage_twitter(req, res, io)
        });
        api.route('/send_email_in').post((req, res) => {
            blending.send_email_in(req, res, io)
        });
        api.route('/send_facebook_messenger').post((req, res) => {
            blending.send_facebook_messenger(req, res, io)
        });
        api.route('/send_facebook_feed').post((req, res) => {
            blending.send_facebook_feed(req, res, io)
        });
        api.route('/send_instagram_messenger').post((req, res) => {
            blending.send_instagram_messenger(req, res, io)
        });
        api.route('/send_instagram_feed').post((req, res) => {
            blending.send_instagram_feed(req, res, io)
        });
        api.route('/send_message_telegram').post((req, res) => {
            blending.send_message_telegram(req, res, io)
        });
    });
}